set number
set autoindent
set smartindent
set showmatch
set ignorecase
set smartcase
set smarttab
set hlsearch
set incsearch
set ruler
set incsearch
set wildmenu
set cursorcolumn
set colorcolumn=80
set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]
set background=dark
set visualbell
syntax enable
set t_Co=256
colorscheme solarized
let g:rehash256 = 1
